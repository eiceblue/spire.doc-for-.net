Word Library-Spire.Doc for .NET
============
________________________________________________________________________________________________________________________

Spire.Doc for .NET enables developers to perform a large range of tasks on Word document(from Version Word97-2003 to Word 2010) for .NET in C# and Visual Basic. This libray is specially designed for .NET developers to help them to create any WinForm and ASP.NET Web applications to create, open, write, edit, save and convert Word document without Microsoft Office and any other third-party tools installed on system. 

Functions
============
------------------------------------------------------------------------------------------------------------------------

Document Conversion

Conversion is one of the featured functions of Spire.Doc for .NET. It enables developers to easily and quickly convert Word to other popular document formats with high fidelity, such as Word to PDF, EPub, HTML, RTF, XML, TXT and Image formats(.tiff, .png, .jpg, .bmp). Also, HTML, RTF and XML can be converted back to Word as well.

Document Mail Merge

Mail Merge is the other featured function of Spire.Doc for .NET. This function eanbles developers to create items, records and reports in batch at once through mail merge. 

Document Security

Spire.Doc for .NET eables developers to protect Word document with encyrpting with password or specified permissions, such as read-only, only comments, only formfield, only revision etc. Also, it can decrypt Word document to remove all protection types. 

Document and Object Operation

Spire.Doc for .NET enables developer to open, create, write, edit and save Word document. Also, it can process all objects in the documents, including section, paragrpah, table, text, image, hyperlink, comment, textbox, bookmark, header/footer, footnote/endnote. These objects can be inserted, edited, formatted and removed. Also, some objects can be extracted and saved in other formats. 

Document Format

Spire.Doc for .NET enables developers to format Word document, including text, paragraph and page. With Spire.Doc for .NET, developers can set font name, style, color, size and highlight color for text; paragrap indent, bulltes, number list format, spacing for paragraph; page background(page border and watermark) and page setup(page margins, orientation, breaks etc.) for page. 


